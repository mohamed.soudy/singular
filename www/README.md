# The Project

<font size ="5">Current rejuvenation strategies, which range from calorie restriction to in vivo partial reprogramming, only improve a few specific cellular processes . In addition, the molecular mechanisms underlying these approaches are largely unknown, which hinders the design of more holistic cellular rejuvenation strategies.To address this issue, we developed SINGULAR (Single-cell RNA-seq Investigation of Rejuvenation Agens and Longevity), a cell rejuvenation atlas that provides a unified system biology analysis of diverse rejuvenation strategies across multiple organs at single-cell resolution. In particular, we leverage network biology approaches to characterize and compare the effects of each strategy at the level of intracellular signaling, cell-cell communication, and transcriptional regulation. As a result, we identified master regulators orchestrating the rejuvenation response and propose that targeting a combination of them leads to a more holistic improvement of dysregulated cellular processes. Thus, the interactive database accompanying SINGULAR is expected to facilitate future design of synthetic rejuvenation interventions.</font>

<p align = "center">
  <img src="https://i.ibb.co/vm7T2jY/imgpsh-fullsize-anim.png" width="900px" height="600px"">
</p>


# The Team

**Principel Investigator: Prof. Dr. Antonio del Sol Mesa**
Computational Biology, Luxembourg Centre for Systems Biomedicine, University of Luxembourg

email: antonio.delsol@uni.lu


**Sascha Jung, Ph.D.**
Computational Biology Lab, Bizkaia Science and Technology Park, building 801A, Derio (Bizkaia)

email: sjung@cicbiogune.es


**Javier Arcos Hodar**
Computational Biology Lab, Bizkaia Science and Technology Park, building 801A, Derio (Bizkaia)

email: jarcos@cicbiogune.es

**Sybille BARVAUX**
Computational Biology, Luxembourg Centre for Systems Biomedicine, University of Luxembourg

email: sybille.barvaux@uni.lu

**Mohamed Soudy**
Computational Biology, Luxembourg Centre for Systems Biomedicine, University of Luxembourg

email: mohamed.soudy@uni.lu

# The Code and Meta-data
- [Souce-code](https://git-r3lab.uni.lu/mohamed.soudy/singular)
- [Data](https://git-r3lab.uni.lu/mohamed.soudy/singular/Visualization-data)

# The Citations

- Ma, S. et al. Caloric Restriction Reprograms the Single-Cell Transcriptional Landscape of Rattus Norvegicus Aging. Cell 180, 984-1001.e22 (2020).
- Ma, S. et al. Heterochronic parabiosis induces stem cell revitalization and systemic rejuvenation across aged tissues. Cell Stem Cell 29, 990-1005.e10 (2022).
- Sun, S. et al. A single-cell transcriptomic atlas of exercise-induced anti-inflammatory and geroprotective effects across the body. The Innovation 4, 100380 (2023).
- Ximerakis, M. et al. Heterochronic parabiosis reprograms the mouse brain transcriptome by shifting aging signatures in multiple cell types. Nat Aging 3, 327–345 (2023).
- Roux, A. E. et al. Diverse partial reprogramming strategies restore youthful gene expression and transiently suppress cell identity. Cell Systems 13, 574-587.e11 (2022).
- Hishida, T. et al. In vivo partial cellular reprogramming enhances liver plasticity and regeneration. Cell Reports 39, 110730 (2022).
- Pálovics, R. et al. Molecular hallmarks of heterochronic parabiosis at single-cell resolution. Nature 603, 309–314 (2022).
- Dharmaratne, M., Kulkarni, A. S., Taherian Fard, A. & Mar, J. C. scShapes: a statistical framework for identifying distribution shapes in single-cell RNA-sequencing data. GigaScience 12, giac126 (2022).
- Choi, J. et al. Intestinal stem cell aging at single‐cell resolution: Transcriptional perturbations alter cell developmental trajectory reversed by gerotherapeutics. Aging Cell 22, e13802 (2023).
